import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    darkMode: localStorage.getItem('mode') ? localStorage.getItem('mode') : null,
    countries: localStorage.getItem('countries') ? JSON.parse(localStorage.getItem('countries')) : [],
    loadingState: false,
    countriesCode: localStorage.getItem('countriesCode') ? JSON.parse(localStorage.getItem('countriesCode')) : [],
  },

  mutations: {
    setMode(state, payload) {
      state.darkMode = payload;
    },

    setCountries(state, payload) {
      const data = [];

      payload.forEach((item) => {
        data.push({
          cioc: item.cioc || 'Unknown',
          name: item.name,
          population: item.population.toLocaleString(),
          region: item.region,
          subRegion: item.subregion,
          nativeName: item.nativeName,
          capital: item.capital,
          flag: item.flag,
          currencies: item.currencies.map((ele) => (
            ele.code
          )),
          languages: item.languages.map((ele) => (
            ele.name
          )),
          topLevelDomain: item.topLevelDomain,
          borders: item.borders,
        });
      });

      state.countries = data;
      localStorage.setItem('countries', JSON.stringify(state.countries));

      state.countriesCode = data.map((ele) => ({
        name: ele.name,
        code: ele.cioc,
      }));
      localStorage.setItem('countriesCode', JSON.stringify(state.countriesCode));

      state.loadingState = true;
    },

    searchCountries(state, payload) {
      state.countries = state.countries.filter((item) => item.name.toLowerCase().match(payload.toLowerCase()));
    },

    filterCountries(state, payload) {
      state.countries = state.countries.filter((item) => item.region.toLowerCase() === payload.toLowerCase());
    },
  },

  actions: {
    // Dark Mode
    enableDarkMode({ commit }) {
      commit('setMode', true);
      localStorage.setItem('mode', true);
    },

    disableDarkMode({ commit }) {
      commit('setMode', false);
      localStorage.removeItem('mode');
    },

    // Countries
    async requestCountries({ commit }) {
      const res = await axios.get('https://restcountries.eu/rest/v2/all');
      commit('setCountries', res.data);
    },

    async searchCountry({ commit }, str) {
      const res = await axios.get(`https://restcountries.eu/rest/v2/name/${str}`);
      commit('setCountries', res.data);
    },

    searchWithSelect({ commit }, str) {
      commit('searchCountries', str);
    },

    filterCountry({ commit }, str) {
      commit('filterCountries', str);
    },
  },
});
