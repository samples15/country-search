module.exports = {
  // Register all css helpers globally
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/styles/_index.scss";
        `,
      },
    },
  },
  // Handle Svg Loader
  chainWebpack: (config) => {
    config.module.rules.delete('svg');
  },
  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.svg$/,
          loader: 'vue-svg-loader',
        },
      ],
    },
  },
};
